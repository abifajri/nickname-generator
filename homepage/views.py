from django.shortcuts import render
from django.http import HttpResponseRedirect
import requests

# Create your views here.
def index(request):
    return render(request, 'index.html')

def generate(request):
    if request.method == "POST":
        # local goes here
        # url = 'http://127.0.0.1:8001/generate/'
        url = 'https://abifajri-nickgenerator-rest.herokuapp.com/generate/'

        response = requests.get(url, data=None)
        
        answer = response.content.decode("utf-8")
    else:
        answer = "Couldn't get the result, please try again!"

    return render(request, 'result.html', {"answer":answer})
